﻿using ConvertFile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pick_Phone_CCBS
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            try
            {
                Application.Run(new Form1());
            } catch (Exception ex)
            {
                ExceptionHandler.writeErrorFile(ex);
            }
            
            //for (int i = 0; i < 10; i++)
            //{
            //    string base64Png = "iVBORw0KGgoAAAANSUhEUgAAAMgAAAAyCAYAAAAZUZThAAAJ+UlEQVR42u1dC5RVVRn+RxQYcBlgLsoXGGVGpuRiZbpyKfZAW4iviAjKF4IhoBFZIoaEpZWCLQJjAS5cJJQRj9ZEJBbhC+2BD/CZCiqhDEEyKiAzA/1f5ztrDpdzz97n3MPMvTP/v9bHDHPPPeeeff5v/8+9r4jIPoPBUBTBPyYmJgeIEcTExAhiYmIEMTExgpiYGEHKWA6xITCCmBwo1YpPKc5QfEtxteJkGxYjiInIsYrRij8pdiv+o9ihaCQaFLsUzyjuMuIYQdoaOSYrXpN0xaXZio42fEaQ1i7XKTZItgrsw0YSI0hrlo9RyRske5vCfMc1utgwtx2CVPNnb8WPFCsUb9Nv38Of+P9aRY1ijmKwon2ZDs44xXYpvZcHgf1FvP+i/T4mrZMgVYpOiqESZHaezqBAq8t0cNYo9jo++3TFVsdxj3qMgUkrJAhm/tMUDyheLmGGfbcgKIZVmURcojiCr4GIZx/EwaiKWEFca6TiNsWrCYH4EfxZcseoSesjyAmKhXSfsirGv2l9zqCigSz1EqRN31Q8pbibbtupfA9Sq79XXE5FLkWg4MMVf+C1Psy/t2N8cDQ/Y9xnP0uCwuFAI4gRJE76KWozKAN88ZmK43mezyv+THLEHQ/C/F0x3nHeuGB6ZcznPpSW6Y8Jn6935PiTGUMgJrlXsVxRJ01V9f5GECNIN8UAxTeY3YF8nbN53AO/RTFXsUSxVDGPLtOFiq6RoP6bKUiWNZs0uuBeLlBsTjh+fMrExHDH9VFD+b7idgdBeikuU0whvkoLZlIBBMGM+2RESVEbWJXwwKs84pdBdKMO9oowWKaPRKzBSkdA7SNwwTozBpvjuP55fM+1Ccf8nGMRupeYNJ5V3KM402M8TVqYIBNyVFi4MDco/uaRJcoLa2gFf+pJpCQ5i8o+nDHY6+JXB0kaw/eK/B0W+hXGaSZlTJDzaUEac1Ta+oTXtiiGcJaG7/+2x/lcyQIo8xsJr4/wHLSvKdZ53MPvGO+E8uMSx2uRWGW+bAnyAcYPc5thtocb16dgxn7I00okvd6YYLFWpBi0EY60NqzBTTHvm5bD2Cw1nS3/LBYyOu+XSICk12cVXO9Dil97nHdSQiYsCdsj6V0fgSXY5rg/ZOUGF7xvRk4TyEWmt5WR5v1liofayFTt9YrHHMcOKLjOYR6B8D5m11ZlULghKe/7sySJT9cA6jWHe44X3L9rFN3pWs4zK9J2CIJi23clWGzkqrofGXOtWeJXSxibMjO2KMN9t6fSI5M1VbEpMgnEXaPGY7xQJO3kOb51preVQZAkl+HNIv7+Tg/3LE5mehIErtJvxa+6v6UIGdMIZvo7ErJQIQY7xivN+DaY3lYGQe5KeOAgB5r4NmZI5fZhujXsi6pOQRDUCy5VvOBxPDJjx8XcF2KeLytGMR2N2OZGaSqQFsqnFY84rlXjGK+042tSAQS5LeEB1kaIsUfSVcGRjfqNBEWyHyiuUiwT/3YNKP18T2LGdRJfyFhmA11D3Ms7jDs6xBxf7RGA1znGK+34mlQAQSYmPEAUxV6U0hYbNVKxoKibUxCkHwnla7kKU7JDi1ig0CLCTeyYYizCSWJiSoWfaASpbIKMSXiAXahoyLg8wYzPKxnTsL6Ae4VWkmniV1gsVnfB7wsc7xkUOf5wJiBcFmRMSoUfYwSpbIIM83iAKDB+RoLc/VD68s87lGlXRoIgdsi6hvyliFWAwl8WyU7FAa3xYZX8k454IUzNDkup8MOMIJVDECj6F2X/bt7zMzzAkyRoGd/nCPAXS9CstzuFkiMVvD7hnC53b2bkc2Lh1hTH9R8l6SeQYEnnvspzvEDSLyl+pXjcCFI5BDlR8Rdp6j3CLL0wwwPsTGvyX2meJsUQ90mwjsN13MX8nFjr0VPxMym9/+wFnu+0hGPOjCQYVnlc8yjT3fIiyNE5Kuv7VNadzUgQWBDUZrY7gnfELt+ToEESPVdXciLISpJdkfjmSnF3OQ+XdF3Rl5gOl08Mck+JWalCzCBJ6pvZmvhkz96SYC36M3SlajNOBOdKsHJycor3zZV0vW4mZUKQHhK0VuSliHAnBnCGftAjOM+6dqRB8ll3Up+CzGhmxMrFdiRHXYrr9OB7jSAVmsUqtZsXrkHY6dqe2SDEONgEYpQ0rfkOW1P+obiT6eK0Cr0nhhwNku+6lih54aItY6Ddmff4V89z7JT9O4Bde2ntkgM7hk3KKM0bCvqfxoq7kxaKWSPFWzZc1muxJK8G3MrfN9MdRI/UAirsIloqVOiXkHBQrs9J0M07irEHMmz/lGz1GigzGjHRwYsiH3Y7+agENZKXIqSEtbw2JtDGRGFLa1shQaLSienMCXLgpg03K/rS7YgK6hco8PUi2Y6Sph1D0MaBOsN4hwVZzOMREGMzidP5fygctvZBb9cnZP91H4cl3EcVY4fzGLDPYdq1LiNxwjX8k0nM3rw+LOfZjFX6M0hHehlLg2/ltQfzfrqbvlY+QXwFO5t8h64a1nRvZCC8gbP7TySoBdQwvbze4W7c3oyf/URazAeYFcvqrsEtQ48Xvk7hvcjfMCY7+PdNvHd89QKKkUOZLu5mutu6CYIvo9ki+W3a0KeFBhCu3w3MdsURIO9NKfbQXYPreLkEG+4dI/FNlGmkmsSD9b+YSYJzFKcwxd+pjbp/LUaQ9pwV81CaB8tgIGER0QU8i67g5oSsXN4Jgmgchj241tK6Ieb6Bd26sXQ9B9Clg4uHroaRjLseZjobqz7RN7eO58HYYjHXNUyiHGIEaT7Bg1otpW1dWssHVw5yKC1Kf7qPs5kgWMrkwEIG8PdSYftTQSfRfVou+daZ0mbgfICtmq6n1cpTujHm6yFNS46r2jpBQoUaLdl2hcfGCMeW+azWgcp0EpMQYYNju5hkRdTdwSKsj0vQgnIB3anxDN5BuucYv7QEoUIriDgJSxpWkvRTmZwB6b8iwbKD0EWLcwGx1/IgPn9MEuiFu1+CLWFXcEKZwnOd0BYJIpFZoppxxDgO0nq6DLsj2MrZeAoHvq0LCIZU8nxxL/mNKna9JG+BdDB3uNxC9/MtYpsUL77u5XOvZYyHpM0VTJK0GYKY5GeNsUwYX1y0mPHEk4wjsEJzFWM+BPeoy/Sl5UUKvCeTJudwth5JS4Bs4jxmEtcwObBDWrYF6F1pSpnfTXdvIC0tLO4HEyyzEcQsyv+XJfRkRqofs1KIdfKso4SdD99mMmCblFcvHSwP0u9djCAmLSkdGUOey/hoAS3Nv+hG1ZI8O6Sp7Qc7ZGI7I+w18EO6y7cy27aQhEMnw0YpbfXpXik99W0EMclNOjMRgR40tPWjNnSLBC03V9MFQi0F2yx1TZGh6sAgHy7gFyTYV206EzQbE2KvvYxTq/IiiMFgiMH/APeqNmwZKzVlAAAAAElFTkSuQmCCDQoNCg0K";
            //    Console.WriteLine(CaptchaUtils.CaptchaToWords(base64Png));
            //}
        }
    }
}
