﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Pick_Phone_CCBS
{
    internal class StringUtils
    {
        public static string extractFromTo(string data, string from, string to, int index_need_to_get)
        {
            string response = "";
            string regexString = string.Format("(?<={0}).*?(?={1})", from, to);
            var res = Regex.Matches(data, regexString, RegexOptions.Singleline);

            if (res != null && res.Count > 0)
            {
                response = res[index_need_to_get].ToString();
            }
            return response;
        }

        public static string getBetween(string strSource, string strStart, string strEnd)
        {
            if (strSource.Contains(strStart) && strSource.Contains(strEnd))
            {
                int Start, End;
                Start = strSource.IndexOf(strStart, 0) + strStart.Length;
                End = strSource.IndexOf(strEnd, Start);
                return strSource.Substring(Start, End - Start);
            }

            return "";
        }
    }
}
