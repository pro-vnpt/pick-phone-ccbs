﻿using Microsoft.ML.OnnxRuntime.Tensors;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;

namespace CTCDecode
{
    class CtcDecoder
    {
        private static int BlankTokenId = 0; // Assuming the blank token has an ID of 0

        public static string Decode(Tensor<float> outputTensor)
        {
            var shape = outputTensor.Dimensions;
            int batchSize = shape[0];
            int sequenceLength = shape[1];
            int numClasses = shape[2];

            // Get the probability matrix as a 3D float array
            float[,,] probabilityMatrix = new float[batchSize, sequenceLength, numClasses];
            CopyTensorValues(outputTensor, probabilityMatrix);

            // Decode the CTC output to get the final predicted text
            string decodedText = CtcDecode(probabilityMatrix);

            return decodedText;
        }

        private static void CopyTensorValues(Tensor<float> tensor, float[,,] targetArray)
        {
            var shape = tensor.Dimensions;
            int batchSize = shape[0];
            int sequenceLength = shape[1];
            int numClasses = shape[2];

            for (int b = 0; b < batchSize; b++)
            {
                for (int t = 0; t < sequenceLength; t++)
                {
                    for (int c = 0; c < numClasses; c++)
                    {
                        targetArray[b, t, c] = tensor[b, t, c];
                    }
                }
            }
        }


        // Helper function to convert token IDs to text
        private static string TokensToText(List<int> tokens)
        {
            // Assuming you have a mapping from token IDs to characters/words
            // For example, if you have a list of characters, you can use:
            char[] characterMap = { '2', '6', 'f', 'r', 'p', 'd', 'y', '7', '8', 'm', '5', 'a', 'e', 'k', 'x', 'g', 'n', '4', '3',
            'c', 'h', 'w', 'b'};

            return new string(tokens.Where(tokenId => tokenId < characterMap.Length).Select(tokenId => characterMap[tokenId]).ToArray());
        }

        private static string CtcDecode(float[,,] probabilityMatrix)
        {
            int sequenceLength = probabilityMatrix.GetLength(1);
            int numClasses = probabilityMatrix.GetLength(2);

            // Initialize the list to store the best path
            var bestPath = new System.Collections.Generic.List<int>();

            // Iterate through the time steps to find the best path
            for (int t = 0; t < sequenceLength; t++)
            {
                // Find the most probable token at each time step
                int bestTokenId = 0;
                float maxProbability = probabilityMatrix[0, t, 0];

                for (int c = 1; c < numClasses; c++)
                {
                    if (probabilityMatrix[0, t, c] > maxProbability)
                    {
                        maxProbability = probabilityMatrix[0, t, c];
                        bestTokenId = c;
                    }
                }

                // Ignore blank tokens and consecutive duplicate tokens
                if ((bestPath.Count == 0 || bestTokenId != bestPath[bestPath.Count - 1]))
                {
                    bestPath.Add(bestTokenId);
                }
            }

            // Convert the token IDs to characters/words using the CTC decoding strategy
            string decodedText = TokensToText(bestPath);

            return decodedText;
        }
    }
}
