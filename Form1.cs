﻿using ConvertFile;
using DeviceId;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pick_Phone_CCBS
{
    public partial class Form1 : Form
    {
        Random random = new Random();
        ISubscriber subScriber;
        string publishChannel;
        string redisConnectionString;
        HttpClient provnptClient = new HttpClient();
        List<CcbsClient> ccbsClients = new List<CcbsClient> ();
        int timeout_by_second = 300;
        int current_ccbs_client = 0;
        private long request_count = 0;
        public Form1()
        {
            InitializeComponent();
            this.Text = AppConfig.APP_NAME + " " + AppConfig.APP_VERSION;
            readSetting();
            readCount();
            readAccounts();
        }

        private async void btn_start_Click(object sender, EventArgs e)
        {
            Invoke(new Action(() =>
            {
                btn_start.Enabled = false;
                btn_stop.Enabled = true;
            }));
            saveSetting();

            if(ccbsClients.Count == 0)
            {
                AddText(tb_logs, string.Format("[{0}]. Không có user nào đã đăng nhập", DateTime.Now.ToString("HH:mm:ss.ff")));
            }

            for (int i = 0; i < ccbsClients.Count; i++)
            {
                int temp = i;
                ccbsClients[temp].isAbleToRun = true;
                new Thread(() =>
                {
                    while (btn_stop.Enabled)
                    {
                        string[] phones = tb_phones.Text.Split(
                            new string[] { Environment.NewLine },
                            StringSplitOptions.RemoveEmptyEntries
                        );
                        if (phones.Length > 0)
                        {
                            for (int j = 0; j < phones.Length; j++)
                            {
                                if (!tb_success.Text.Contains(phones[j]))
                                {
                                    PickPhoneCCBS(ccbsClients[temp], phones[j]);
                                }
                                Thread.Sleep(int.Parse(tb_delay.Text));
                            }
                        } else
                        {
                            Thread.Sleep(100);
                        }
                    }
                })
                { IsBackground = true }.Start();
            }
        }

        void onEmptyPhone()
        {
            this.Invoke((MethodInvoker)delegate {
                btn_start.Enabled = true;
                btn_stop.Enabled = false;
            });
        }

        private async void PickPhoneCCBS(CcbsClient ccbsClient, string phone)
        {
            // check qua so luong dang ky
            if (!ccbsClient.isAbleToRun) return;

            // check limit
            if (ck_request_limit.Checked)
            {
                long requestLimit = long.Parse(tb_request_limit.Text.Trim());
                if (ccbsClient.requestCount >= requestLimit)
                {
                    AddText(tb_logs, string.Format("[{0}].{1} pick {2}. Tình trạng {3}", DateTime.Now.ToString("HH:mm:ss.ff"), ccbsClient.username, phone, "Đã quá giới hạn request"));
                    return;
                }
            }
            string captcha = GetCaptchaAsync1(ccbsClient.httpClient).Result;
            if(captcha.Length!=5)
            {
                AddText(tb_logs, string.Format("[{0}]. Captcha {1}. Tình trạng {2}", DateTime.Now.ToString("HH:mm:ss.ff"), captcha, "Skip captcha sai"));
                return;
            }
            string userip = tb_user_ip.Text;
            Stopwatch sw = new Stopwatch();
            sw.Start();
            InCreaseRequestCount();
            IncreaseUserRequestCount(ccbsClient);
            string url = string.Format("http://{0}/ccbs/captcha/valid.jsp?j_captcha_response={1}&somay={2}&userip={3}&random={4}", tb_host.Text ,captcha, phone, userip, DateTime.Now.Ticks);
            string response = await HttpClientUtil.CcbsGET(ccbsClient.httpClient, HttpMethod.Get, url, null);
            AddText(tb_logs, string.Format("[{0}]. Process {1}s. {2} pick {3}. Tình trạng {4}", DateTime.Now.ToString("HH:mm:ss.ff"), sw.ElapsedMilliseconds / 1000.0, ccbsClient.username, phone, response.Trim()));
            if (response.Contains("So thue bao da dang ky hoac khong ton tai trong kho"))
            {
            }
            else if (response.Trim() == "1")
            {
                AddTextNotDupplicate(tb_success, phone);
                RemovePhoneFromTextBox(tb_phones, phone);
                SendLogBatSo(phone, ccbsClient.username);
                PublishMessage($"{phone} | THANH CONG");
            } else if (response.Trim().Contains("unique constraint"))
            {
                RemovePhoneFromTextBox(tb_phones, phone);
                PublishMessage($"{phone} | DA BI GIU");
            } else if (response.Trim().Contains("Vui long giam so luong dk"))
            {
                ccbsClient.isAbleToRun = false;
            }
            // GetAndSetCompleteCaptcha(ccbsClient);
        }

        private async void GetAllCaptcha()
        {
            for(int i =0; i < ccbsClients.Count; i++)
            {
                int temp = i;
                new Thread(() =>
                {
                    GetAndSetCompleteCaptcha(ccbsClients[temp]);
                })
                { IsBackground = true }.Start();
            }
        }

        private void GetAndSetCompleteCaptcha(CcbsClient ccbsClient)
        {
            string captcha = GetCaptchaAsync1(ccbsClient.httpClient).Result;
            while(captcha.Length != 5)
            {
                captcha = GetCaptchaAsync1(ccbsClient.httpClient).Result;
            }
            ccbsClient.captcha = captcha;
        }

        public void AddTextNotDupplicate(TextBox textBox, string text)
        {
            Invoke(new Action(() =>
            {
                if (!textBox.Text.Contains(text))
                {
                    textBox.AppendText(text);
                    textBox.AppendText(Environment.NewLine);
                }
            }));
        }

        private void InCreaseRequestCount()
        {
            Invoke(new Action(() =>
            {
                request_count++;
                lb_request_count.Text = $"{request_count}";
            }));
        }

        private async Task<string> GetCaptchaAsync1(HttpClient client)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            MemoryStream memoryStream = GetCaptchaStreamAsync(client).Result;
            try
            {
                byte[] imageBytes = memoryStream.ToArray();
                string base64 = Convert.ToBase64String(imageBytes);
                
                string captcha = CaptchaUtils.CaptchaToWords(base64);
                if (cb_show_captcha.Checked)
                {
                    AddText(tb_logs, string.Format("[{0}]. Process {1}s. Captcha {2}", DateTime.Now.ToString("HH:mm:ss.ff"), sw.ElapsedMilliseconds / 1000.0, captcha));
                }
                return captcha;
            }
            catch (Exception ex)
            {
                ExceptionHandler.writeErrorFile(ex);
                return "";
            }
        }

        public void readSetting()
        {
            string fileName = @"app/setting.txt";
            string jsonString = "";
            try
            {
                jsonString = File.ReadAllText(fileName);
            }
            catch (Exception ex)
            {
                ExceptionHandler.writeErrorFile(ex);
            }
            if (string.IsNullOrEmpty(jsonString)) return;
            string[] data = jsonString.Split('-');
            tb_delay.Text = data[0];
            tb_host.Text = data[1];
            tb_user_ip.Text = data[2];
            tb_broadcast_server.Text = data[3];
            tb_broadcast_channel.Text = data[4];
            tb_request_limit.Text = data[5];
        }

        public void saveSetting()
        {
            string json = string.Format("{0}-{1}-{2}-{3}-{4}-{5}", tb_delay.Text, tb_host.Text, tb_user_ip.Text, tb_broadcast_server.Text, tb_broadcast_channel.Text, tb_request_limit.Text);
            File.WriteAllText("app/setting.txt", json);
        }


        private async Task<MemoryStream> GetCaptchaStreamAsync(HttpClient client)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            response.Content = new StringContent("Init content response");
            string url = "http://" + tb_host.Text + "/ccbs/captcha/img.jsp?random=" + DateTimeOffset.Now.ToUnixTimeMilliseconds();
            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri(url),
                Content = null
            };
            //client.DefaultRequestHeaders.Add("Referer", "http://ccbs.vnpt.vn/ccbs/main?1iutlomLork=VZZH5jqeinutyu%7Fk%7Big%7B5otjk~");
            //client.DefaultRequestHeaders.Add("Accept", "*/*");
            try
            {
                response = await client.SendAsync(request);
            } catch(Exception ex) 
            {
                ExceptionHandler.writeErrorFile(ex);
            }
            //client.DefaultRequestHeaders.Remove("Referer");
            //client.DefaultRequestHeaders.Remove("Accept");
            await response.Content.LoadIntoBufferAsync();
            var loginResponseString = await response.Content.ReadAsStreamAsync();
            return (MemoryStream)loginResponseString;
        }

        public void AddText(TextBox textBox, string text)
        {
            Invoke(new Action(() =>
            {
                if (textBox.Text.EndsWith(Environment.NewLine) || string.IsNullOrEmpty(textBox.Text))
                {
                    textBox.AppendText(text);
                    textBox.AppendText(Environment.NewLine);
                } else
                {
                    textBox.AppendText(Environment.NewLine);
                    textBox.AppendText(text);
                    textBox.AppendText(Environment.NewLine);
                }
                
            }));
        }

        private void btn_stop_Click(object sender, EventArgs e)
        {
            Invoke(new Action(() =>
            {
                btn_start.Enabled = true;
                btn_stop.Enabled = false;
            }));
            saveAccounts();
            saveCount();
        }

        private async void Form1_Load(object sender, EventArgs e)
        {
            string deviceId = new DeviceIdBuilder()
            .OnWindows(windows => windows
                .AddProcessorId()
                .AddMotherboardSerialNumber()
                .AddSystemDriveSerialNumber())
            .ToString();
            File.WriteAllText("app/key.txt", deviceId);
            var handler = new HttpClientHandler() { UseProxy = false };
            HttpClient client = new HttpClient(handler);
            string content = $@"{{
                                          ""app"": ""{AppConfig.APP_CODE}"",
                                          ""key"": ""{deviceId}""
                                        }}";
            var res = await HttpClientUtil.callAPI(client, HttpMethod.Post, "http://xacthuc.provnpt.com/api/v1/auth-app", content);
            if (res.Contains("\"active\":true"))
            {
                return;
            }
            else
            {
                MessageBox.Show("App chưa được kích hoạt");
                this.Close();
            }
        }

        public void RemovePhoneFromTextBox(TextBox textBox, string phone)
        {
            if (this.Visible)
            {
                Invoke(new Action(() =>
                {
                    // bỏ 2 số đầu đi để cover được 2 dạng: đầu 84, đầu 0
                    textBox.Lines = textBox.Lines.Where(line => !line.Contains(phone.Substring(2))).ToArray();
                }));
            }
        }

        private void onClickConnectBroadcast(object sender, EventArgs e)
        {
            onSubcribe();
        }

        void onSubcribe()
        {
            redisConnectionString = $"provnpt.com:{tb_broadcast_server.Text}";
            publishChannel = tb_broadcast_channel.Text;
            ConnectionMultiplexer redis = ConnectionMultiplexer.Connect(redisConnectionString);
            subScriber = redis.GetSubscriber();

            subScriber.Subscribe(publishChannel, (channel, message) =>
            {
                AddText(tb_notify, $"[{DateTime.Now:HH:mm:ss.fff}]: {message}");
                handleMessage(message);
            });

            // check after action
            if (subScriber.IsConnected())
            {
                AddText(tb_notify, $"[{DateTime.Now:HH:mm:ss.fff}]: Connected channel {publishChannel}");
                Invoke(new Action(() =>
                {
                    btn_connect_broadcast.Enabled = false;
                    btn_disconnect_broadcast.Enabled = true;
                }));
            }
        }

        private void onClickDisconnectBroadcast(object sender, EventArgs e)
        {
            onUnsubcribe();
        }


        void onUnsubcribe()
        {
            subScriber.Unsubscribe(publishChannel, (channel, message) =>
            {
                // no action
            });

            // check after action
            if (subScriber.IsConnected())
            {
                AddText(tb_notify, $"[{DateTime.Now:HH:mm:ss.fff}]: Disconected channel {publishChannel}");
                Invoke(new Action(() =>
                {
                    btn_connect_broadcast.Enabled = true;
                    btn_disconnect_broadcast.Enabled = false;
                }));
            }
        }

        void handleMessage(string rawMessage)
        {
            char[] spearator = { '|' };
            string[] messages = rawMessage.Split(spearator, StringSplitOptions.None);

            string phone = messages[0].Trim();
            string command = messages[1].Trim();
            if(command.Equals("THANH CONG") | command.Equals("THAT BAI") || command.Equals("DA BI GIU"))
            {
                RemovePhoneFromTextBox(tb_phones, phone);
            } else if (command.Equals("TRONG KHO") || command.Equals("UU TIEN"))
            {
                // add vao list so dang trong kho 10 lan, random
                for(int i = 0; i < 10; i++)
                {
                    AddText(tb_phones, phone);
                }
            } else if (command.Equals("GIAM UU TIEN"))
            {
                if (tb_phones.Text.Contains(phone))
                {
                    RemovePhoneFromTextBox(tb_phones, phone);
                    AddText(tb_phones, phone);
                }
            } else if (command.Equals("THEM SO"))
            {
                if (!tb_phones.Text.Contains(phone))
                {
                    AddText(tb_phones, phone);
                }   
            } else if (command.Equals("THAY SO"))
            {
                // Thay toàn bộ list số bằng 1 số mới
                ReplaceAllText(tb_phones, phone);
            } else if (command.Equals("CCBS START"))
            {
                btn_start_Click(null, null);
            }
            else if (command.Equals("CCBS STOP"))
            {
                btn_stop_Click(null, null);
            } else if (command.Equals("CCBS LOGIN"))
            {
                btn_login_Click(null, null);
            } else if (command.Equals("CCBS PICK ONE"))
            {
                handleCcbsPickOne(phone);
            }
        }

        void handleCcbsPickOne(string phone)
        {
            new Thread(() =>
            {
                if (ccbsClients.Count == 0)
                {
                    AddText(tb_logs, string.Format("[{0}]. Không có user nào đã đăng nhập", DateTime.Now.ToString("HH:mm:ss.ff")));
                    return;
                }
                if(current_ccbs_client >= ccbsClients.Count)
                {
                    current_ccbs_client = 0;
                } else if (current_ccbs_client==0)
                {
                    //nothing
                }
                else
                {
                    current_ccbs_client++;
                }
                new Thread(() =>
                {
                    PickPhoneCCBS(ccbsClients[current_ccbs_client], phone);
                })
                { IsBackground = true }.Start();
            })
            { IsBackground = true }.Start(); 
        }

        void AddTextAtRandomLine(TextBox textbox, string text)
        {
            // nếu như ko có số nào thì thêm 1
            if (!string.IsNullOrEmpty(textbox.Text))
            {
                AddText(textbox, text);
            }  else // không thì thêm random
            {
                int randomLineIndex = random.Next(0, textbox.Lines.Length);
                Invoke(new Action(() =>
                {
                    string oldText = textbox.Lines[randomLineIndex];

                    // Get the current lines from the TextBox
                    string[] lines = textbox.Lines;

                    // Insert the new text into the randomly chosen line
                    lines[randomLineIndex] = oldText + Environment.NewLine + text;

                    // Update the TextBox with the modified lines
                    textbox.Lines = lines;
                }));
            }
        }

        private async void SendLogBatSo(string phone, string username)
        {
            string request = $@"{{
                                          ""type"": ""CCBS_PLACE_SUCCESS"",
                                          ""username"": ""{username}"",
                                          ""content"": ""{phone}""
                                        }}";
            HttpClientUtil.callAPI(provnptClient, HttpMethod.Post, "http://provnpt.com:8081/api/v1/maxacthuc", request);
        }

        void PublishMessage(string message)
        {
            if(subScriber != null && subScriber.IsConnected())
            {
                new Thread(() =>
                {
                    subScriber.Publish(publishChannel, $"{message}");
                }) { IsBackground = true }.Start();
            }
        }

        void ReplaceAllText(TextBox textBox, string phone)
        {
            Invoke(new Action(() =>
            {
                textBox.Text = phone;
            }));
        }

        private void btn_login_Click(object sender, EventArgs e)
        {
            saveAccounts();
            List<CcbsClient> tempCcbsClients = ccbsClients;
            ccbsClients = new List<CcbsClient>();
            string[] accounts = tb_accounts.Text.Split(
                new string[] { Environment.NewLine },
                StringSplitOptions.RemoveEmptyEntries
            );
                
            new Thread(async () =>
            {
                for (int i = 0; i < accounts.Length; i++)
                {
                    string[] line_data = accounts[i].Split('|');
                    string username = line_data[0].Trim();
                    string password = line_data[1].Trim();
                    string otp = line_data[2].Trim();
                    long requetsCount = 0;
                    requetsCount = long.Parse(line_data[3].Trim());

                    var handler = new HttpClientHandler()
                    {
                        AllowAutoRedirect = false
                    };
                    HttpClient client = new HttpClient(handler);
                    client.Timeout = TimeSpan.FromSeconds(timeout_by_second);
                    string request = "1iutlomLork=gjsot5pl{tizout&1pl{tizout=tku4ysgxz{o4rg\u007fu{z4ykz[ykxVgxgskzkx./&username=" + username + "&password=" + password + "&options=" + otp;
                    var res = await HttpClientUtil.callAPILoginCCBS(client, HttpMethod.Post, "http://" + tb_host.Text + "/ccbs/main", request);
                    if (res == "0")
                    {
                        AddText(tb_logs, string.Format("[{0}]. Đăng nhập thành công {1}", DateTime.Now.ToString("HH:mm:ss.ff"), username));
                        CcbsClient ccbsClient = new CcbsClient();
                        ccbsClient.username = username;
                        ccbsClient.password = password;
                        ccbsClient.otp = otp;
                        ccbsClient.httpClient = client;
                        ccbsClient.requestCount = requetsCount;
                        ccbsClients.Add(ccbsClient);
                    }
                    else
                    {
                        AddText(tb_logs, string.Format("[{0}]. Lỗi đăng nhập {1}. {2}", DateTime.Now.ToString("HH:mm:ss.ff"), username, res));
                    }
                }
                GetAllCaptcha();
            })
            { IsBackground = true }.Start();
        }

        private void btn_reset_request_count_Click(object sender, EventArgs e)
        {
            Invoke(new Action(() =>
            {
                request_count = 0;
                lb_request_count.Text = request_count.ToString();
                saveCount();
            }));
        }

        // Tăng 1 và cập nhật lên giao diện
        private void IncreaseUserRequestCount(CcbsClient ccbsClient)
        {
            ccbsClient.requestCount++;
            string[] accounts = tb_accounts.Text.Split(
                new string[] { Environment.NewLine },
                StringSplitOptions.RemoveEmptyEntries
            );
            string res = "";
            for (int i = 0; i < accounts.Length; i++)
            {
                res += $"{ccbsClients[i].username} | {ccbsClients[i].password} | {ccbsClients[i].otp} | {ccbsClients[i].requestCount}" + Environment.NewLine;
            }
            Invoke(new Action(() =>
            {
                tb_accounts.Text = res;
            }));
        }

        void saveCount()
        {
            File.WriteAllText("app/count.txt", request_count.ToString());
        }

        void readCount()
        {
            try
            {
                string fileName = @"app/count.txt";
                request_count = long.Parse(File.ReadAllText(fileName));
            }
            catch (Exception ex)
            {
                request_count = 0;
            }
            lb_request_count.Text = request_count.ToString();
        }

        void saveAccounts()
        {
            File.WriteAllText("app/account.txt", tb_accounts.Text);
        }

        void readAccounts()
        {
            try
            {
                string fileName = @"app/account.txt";
                string data = File.ReadAllText(fileName);
                if (string.IsNullOrEmpty(data))
                {
                    tb_accounts.Text = "username | password | otp | request_count";
                } else
                {
                    tb_accounts.Text = data;
                }
                
            }
            catch (Exception ex)
            {
                tb_accounts.Text = "username | password | otp | request_count";
            }
            
        }
    }
}
