﻿using System.Net.Http;

namespace Pick_Phone_CCBS
{
    public class CcbsClient
    {
        public string username {  get; set; }
        public string password {  get; set; }
        public string otp {  get; set; }
        public HttpClient httpClient { get; set; }
        public string captcha { get; set; }
        public long requestCount { get; set; }
        public bool isAbleToRun { get; set; }
    }
}
