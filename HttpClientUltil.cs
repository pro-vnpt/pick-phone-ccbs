﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Pick_Phone_CCBS
{
    public class HttpClientUtil
    {
        public static async Task<string> callAPILoginCCBS(HttpClient client, HttpMethod method, string url, String content)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            response.Content = new StringContent("Init content response");
            client.DefaultRequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
            client.DefaultRequestHeaders.Add("User-Agent", "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 10.0; WOW64; Trident/7.0; .NET4.0C; .NET4.0E; Zoom 3.6.0)");
            client.DefaultRequestHeaders.Add("Referer", "http://ccbs.vnpt.vn/ccbs/login.htm");
            var request = new HttpRequestMessage
            {
                Method = method,
                RequestUri = new Uri(url),
                Content = new StringContent(content, Encoding.UTF8, "application/x-www-form-urlencoded")
            };
            try
            {
                response = await client.SendAsync(request);
                var loginResponseString = await response.Content.ReadAsStringAsync();
                string cookie = response.Headers.GetValues("Set-Cookie").FirstOrDefault();
                client.DefaultRequestHeaders.Remove("Referer");
                client.DefaultRequestHeaders.Remove("X-Requested-With");
                client.DefaultRequestHeaders.Add("Cookie", cookie + "; BIGipServerAPP_CCBS=1578540810.20480.0000; " + "__utmz=223467145.1680449803.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); __utma=223467145.1048607736.1680449803.1680449803.1680449803.1; ");
                return loginResponseString;
            } catch (Exception ex)
            {
                return await Task.FromResult("Lỗi không xác định");
            }
        }


        public static async Task<string> CcbsGET(HttpClient client, HttpMethod method, string url, String content)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            response.Content = new StringContent("Init content response");
            var request = new HttpRequestMessage
            {
                Method = method,
                RequestUri = new Uri(url),
                Content = null
            };
            try
            {
                response = await client.SendAsync(request);
            } catch (TaskCanceledException ex)
            {
                return await Task.FromResult("Lỗi custom request timeout");
            } catch (Exception ex)
            {
                return await Task.FromResult("Lỗi không xác định");
            }
            
            var loginResponseString = await response.Content.ReadAsStringAsync();
            return loginResponseString;
        }

        public static async Task<string> callAPI(HttpClient client, HttpMethod method, string url, String content)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            response.Content = new StringContent("Init content response");
            var request = new HttpRequestMessage
            {
                Method = method,
                RequestUri = new Uri(url),
                Content = new StringContent(content, Encoding.UTF8, "application/json")
            };
            try
            {
                response = await client.SendAsync(request);
            }
            catch (Exception)
            {
                return await Task.FromResult("Lỗi không xác định");
            }
            var loginResponseString = await response.Content.ReadAsStringAsync();
            return loginResponseString;
        }
    }
}
