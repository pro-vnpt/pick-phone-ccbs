﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pick_Phone_CCBS
{
    internal class ExceptionHandler
    {
        public static void writeErrorFile(Exception ex)
        {
            string path = $"app/error.txt";
            string message = ex.Message;
            using (var file = File.Exists(path) ? File.Open(path, FileMode.Append) : File.Open(path, FileMode.CreateNew))
            using (var stream = new StreamWriter(file))
                stream.WriteLine($"[{DateTime.Now:dd-MM-yyyy HH:mm:ss.fff}] " + message + Environment.NewLine + ex.StackTrace + "====================================");
        }

        public static void writeErrorMessageFile(String message)
        {
            string path = $"app/error.txt";
            using (var file = File.Exists(path) ? File.Open(path, FileMode.Append) : File.Open(path, FileMode.CreateNew))
            using (var stream = new StreamWriter(file))
                stream.WriteLine($"[{DateTime.Now:dd-MM-yyyy HH:mm:ss.fff}] " + message + Environment.NewLine + "====================================");
        }
    }
}
