﻿namespace Pick_Phone_CCBS
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tb_accounts = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_phones = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tb_success = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tb_logs = new System.Windows.Forms.TextBox();
            this.tb_delay = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tb_host = new System.Windows.Forms.TextBox();
            this.btn_start = new System.Windows.Forms.Button();
            this.btn_stop = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.tb_user_ip = new System.Windows.Forms.TextBox();
            this.cb_show_captcha = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tb_request_limit = new System.Windows.Forms.TextBox();
            this.ck_request_limit = new System.Windows.Forms.CheckBox();
            this.btn_login = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btn_disconnect_broadcast = new System.Windows.Forms.Button();
            this.btn_connect_broadcast = new System.Windows.Forms.Button();
            this.tb_broadcast_channel = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tb_broadcast_server = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tb_notify = new System.Windows.Forms.TextBox();
            this.btn_reset_request_count = new System.Windows.Forms.Button();
            this.lb_request_count = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tb_accounts
            // 
            this.tb_accounts.Location = new System.Drawing.Point(12, 25);
            this.tb_accounts.Multiline = true;
            this.tb_accounts.Name = "tb_accounts";
            this.tb_accounts.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_accounts.Size = new System.Drawing.Size(283, 290);
            this.tb_accounts.TabIndex = 0;
            this.tb_accounts.Text = "username | password | otp | request_count";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Accounts";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(298, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "SĐT";
            // 
            // tb_phones
            // 
            this.tb_phones.Location = new System.Drawing.Point(301, 25);
            this.tb_phones.Multiline = true;
            this.tb_phones.Name = "tb_phones";
            this.tb_phones.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_phones.Size = new System.Drawing.Size(99, 290);
            this.tb_phones.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(403, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Thành công";
            // 
            // tb_success
            // 
            this.tb_success.Location = new System.Drawing.Point(406, 25);
            this.tb_success.Multiline = true;
            this.tb_success.Name = "tb_success";
            this.tb_success.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_success.Size = new System.Drawing.Size(109, 290);
            this.tb_success.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 329);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Logs";
            // 
            // tb_logs
            // 
            this.tb_logs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_logs.Location = new System.Drawing.Point(16, 353);
            this.tb_logs.Multiline = true;
            this.tb_logs.Name = "tb_logs";
            this.tb_logs.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_logs.Size = new System.Drawing.Size(1057, 150);
            this.tb_logs.TabIndex = 6;
            // 
            // tb_delay
            // 
            this.tb_delay.Location = new System.Drawing.Point(64, 19);
            this.tb_delay.Name = "tb_delay";
            this.tb_delay.Size = new System.Drawing.Size(195, 20);
            this.tb_delay.TabIndex = 8;
            this.tb_delay.Text = "2000";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Delay";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 48);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Server";
            // 
            // tb_host
            // 
            this.tb_host.Location = new System.Drawing.Point(64, 45);
            this.tb_host.Name = "tb_host";
            this.tb_host.Size = new System.Drawing.Size(195, 20);
            this.tb_host.TabIndex = 10;
            this.tb_host.Text = "ccbs.vnpt.vn";
            // 
            // btn_start
            // 
            this.btn_start.Location = new System.Drawing.Point(175, 179);
            this.btn_start.Name = "btn_start";
            this.btn_start.Size = new System.Drawing.Size(75, 23);
            this.btn_start.TabIndex = 12;
            this.btn_start.Text = "Start";
            this.btn_start.UseVisualStyleBackColor = true;
            this.btn_start.Click += new System.EventHandler(this.btn_start_Click);
            // 
            // btn_stop
            // 
            this.btn_stop.Enabled = false;
            this.btn_stop.Location = new System.Drawing.Point(94, 179);
            this.btn_stop.Name = "btn_stop";
            this.btn_stop.Size = new System.Drawing.Size(75, 23);
            this.btn_stop.TabIndex = 13;
            this.btn_stop.Text = "Stop";
            this.btn_stop.UseVisualStyleBackColor = true;
            this.btn_stop.Click += new System.EventHandler(this.btn_stop_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 74);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "User ip";
            // 
            // tb_user_ip
            // 
            this.tb_user_ip.Location = new System.Drawing.Point(64, 71);
            this.tb_user_ip.Name = "tb_user_ip";
            this.tb_user_ip.Size = new System.Drawing.Size(195, 20);
            this.tb_user_ip.TabIndex = 16;
            this.tb_user_ip.Text = "10.24.72.96";
            // 
            // cb_show_captcha
            // 
            this.cb_show_captcha.AutoSize = true;
            this.cb_show_captcha.Location = new System.Drawing.Point(12, 156);
            this.cb_show_captcha.Name = "cb_show_captcha";
            this.cb_show_captcha.Size = new System.Drawing.Size(112, 17);
            this.cb_show_captcha.TabIndex = 23;
            this.cb_show_captcha.Text = "Show log captcha";
            this.cb_show_captcha.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tb_request_limit);
            this.groupBox1.Controls.Add(this.ck_request_limit);
            this.groupBox1.Controls.Add(this.btn_login);
            this.groupBox1.Controls.Add(this.tb_user_ip);
            this.groupBox1.Controls.Add(this.cb_show_captcha);
            this.groupBox1.Controls.Add(this.tb_delay);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.tb_host);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.btn_start);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.btn_stop);
            this.groupBox1.Location = new System.Drawing.Point(807, 107);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(266, 208);
            this.groupBox1.TabIndex = 24;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Setting";
            // 
            // tb_request_limit
            // 
            this.tb_request_limit.Location = new System.Drawing.Point(157, 95);
            this.tb_request_limit.Name = "tb_request_limit";
            this.tb_request_limit.Size = new System.Drawing.Size(103, 20);
            this.tb_request_limit.TabIndex = 26;
            this.tb_request_limit.Text = "1000";
            // 
            // ck_request_limit
            // 
            this.ck_request_limit.AutoSize = true;
            this.ck_request_limit.Checked = true;
            this.ck_request_limit.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ck_request_limit.Location = new System.Drawing.Point(13, 97);
            this.ck_request_limit.Name = "ck_request_limit";
            this.ck_request_limit.Size = new System.Drawing.Size(126, 17);
            this.ck_request_limit.TabIndex = 25;
            this.ck_request_limit.Text = "Request limit / 1 user";
            this.ck_request_limit.UseVisualStyleBackColor = true;
            // 
            // btn_login
            // 
            this.btn_login.Location = new System.Drawing.Point(13, 179);
            this.btn_login.Name = "btn_login";
            this.btn_login.Size = new System.Drawing.Size(75, 23);
            this.btn_login.TabIndex = 24;
            this.btn_login.Text = "Login";
            this.btn_login.UseVisualStyleBackColor = true;
            this.btn_login.Click += new System.EventHandler(this.btn_login_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btn_disconnect_broadcast);
            this.groupBox2.Controls.Add(this.btn_connect_broadcast);
            this.groupBox2.Controls.Add(this.tb_broadcast_channel);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.tb_broadcast_server);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Location = new System.Drawing.Point(807, 25);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(266, 76);
            this.groupBox2.TabIndex = 25;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Broadcast";
            // 
            // btn_disconnect_broadcast
            // 
            this.btn_disconnect_broadcast.Enabled = false;
            this.btn_disconnect_broadcast.Location = new System.Drawing.Point(190, 43);
            this.btn_disconnect_broadcast.Name = "btn_disconnect_broadcast";
            this.btn_disconnect_broadcast.Size = new System.Drawing.Size(69, 23);
            this.btn_disconnect_broadcast.TabIndex = 29;
            this.btn_disconnect_broadcast.Text = "Disconect";
            this.btn_disconnect_broadcast.UseVisualStyleBackColor = true;
            this.btn_disconnect_broadcast.Click += new System.EventHandler(this.onClickDisconnectBroadcast);
            // 
            // btn_connect_broadcast
            // 
            this.btn_connect_broadcast.Location = new System.Drawing.Point(190, 17);
            this.btn_connect_broadcast.Name = "btn_connect_broadcast";
            this.btn_connect_broadcast.Size = new System.Drawing.Size(69, 23);
            this.btn_connect_broadcast.TabIndex = 28;
            this.btn_connect_broadcast.Text = "Connect";
            this.btn_connect_broadcast.UseVisualStyleBackColor = true;
            this.btn_connect_broadcast.Click += new System.EventHandler(this.onClickConnectBroadcast);
            // 
            // tb_broadcast_channel
            // 
            this.tb_broadcast_channel.Location = new System.Drawing.Point(64, 45);
            this.tb_broadcast_channel.Name = "tb_broadcast_channel";
            this.tb_broadcast_channel.Size = new System.Drawing.Size(120, 20);
            this.tb_broadcast_channel.TabIndex = 26;
            this.tb_broadcast_channel.Text = "room";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(9, 48);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(46, 13);
            this.label11.TabIndex = 27;
            this.label11.Text = "Channel";
            // 
            // tb_broadcast_server
            // 
            this.tb_broadcast_server.Location = new System.Drawing.Point(64, 19);
            this.tb_broadcast_server.Name = "tb_broadcast_server";
            this.tb_broadcast_server.Size = new System.Drawing.Size(120, 20);
            this.tb_broadcast_server.TabIndex = 24;
            this.tb_broadcast_server.Text = "6379";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(26, 13);
            this.label7.TabIndex = 25;
            this.label7.Text = "Port";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(518, 9);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(34, 13);
            this.label12.TabIndex = 27;
            this.label12.Text = "Notify";
            // 
            // tb_notify
            // 
            this.tb_notify.Location = new System.Drawing.Point(521, 25);
            this.tb_notify.Multiline = true;
            this.tb_notify.Name = "tb_notify";
            this.tb_notify.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_notify.Size = new System.Drawing.Size(279, 290);
            this.tb_notify.TabIndex = 26;
            // 
            // btn_reset_request_count
            // 
            this.btn_reset_request_count.Location = new System.Drawing.Point(880, 324);
            this.btn_reset_request_count.Name = "btn_reset_request_count";
            this.btn_reset_request_count.Size = new System.Drawing.Size(52, 23);
            this.btn_reset_request_count.TabIndex = 28;
            this.btn_reset_request_count.Text = "Reset request";
            this.btn_reset_request_count.UseVisualStyleBackColor = true;
            this.btn_reset_request_count.Click += new System.EventHandler(this.btn_reset_request_count_Click);
            // 
            // lb_request_count
            // 
            this.lb_request_count.AutoSize = true;
            this.lb_request_count.ForeColor = System.Drawing.Color.Red;
            this.lb_request_count.Location = new System.Drawing.Point(938, 329);
            this.lb_request_count.Name = "lb_request_count";
            this.lb_request_count.Size = new System.Drawing.Size(13, 13);
            this.lb_request_count.TabIndex = 29;
            this.lb_request_count.Text = "0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1089, 512);
            this.Controls.Add(this.lb_request_count);
            this.Controls.Add(this.btn_reset_request_count);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.tb_notify);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tb_logs);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tb_success);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tb_phones);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tb_accounts);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Form 1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tb_accounts;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_phones;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tb_success;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tb_logs;
        private System.Windows.Forms.TextBox tb_delay;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tb_host;
        private System.Windows.Forms.Button btn_start;
        private System.Windows.Forms.Button btn_stop;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tb_user_ip;
        private System.Windows.Forms.CheckBox cb_show_captcha;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btn_disconnect_broadcast;
        private System.Windows.Forms.Button btn_connect_broadcast;
        private System.Windows.Forms.TextBox tb_broadcast_channel;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tb_broadcast_server;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tb_notify;
        private System.Windows.Forms.Button btn_login;
        private System.Windows.Forms.Button btn_reset_request_count;
        private System.Windows.Forms.Label lb_request_count;
        private System.Windows.Forms.TextBox tb_request_limit;
        private System.Windows.Forms.CheckBox ck_request_limit;
    }
}

