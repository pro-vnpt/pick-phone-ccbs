﻿using Microsoft.ML.OnnxRuntime.Tensors;
using Microsoft.ML.OnnxRuntime;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CTCDecode;
using System.IO;
using static System.Net.Mime.MediaTypeNames;
using System.Drawing.Imaging;
using static System.Collections.Specialized.BitVector32;
using Ionic.Zip;
using System.Collections;

namespace ConvertFile
{
    public static class CaptchaUtils
    {
        private static InferenceSession session;

        static CaptchaUtils()
        {

            //string currentDirectory = Directory.GetCurrentDirectory();
            //session = new InferenceSession(currentDirectory + "\\model.onnx");

            string zipFilePath = "model.zip";
            string password = "Phutho@123!@#";

            using (ZipFile zip = new ZipFile(zipFilePath))
            {
                // Set the password for the zip file
                zip.Password = password;

                // Find the entry you want to extract (e.g., the first entry)
                ZipEntry entryToExtract = zip.Entries.FirstOrDefault();

                if (entryToExtract != null)
                {
                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        entryToExtract.Extract(memoryStream);
                        byte[] byteArray = memoryStream.ToArray();

                        session = new InferenceSession(byteArray);
                    }
                }
            }
        }


        public static string CaptchaToWords(string base64Image)
        {
            string base64Png = base64Image;
            try
            {
                byte[] pngBytes = Convert.FromBase64String(base64Png);

                // Load the PNG image from bytes
                using (MemoryStream memoryStream = new MemoryStream(pngBytes))
                {
                    using (Bitmap originalImage = new Bitmap(memoryStream))
                    {
                        // Create a new Bitmap with white background
                        using (Bitmap newImage = new Bitmap(originalImage.Width, originalImage.Height))
                        {
                            using (Graphics graphics = Graphics.FromImage(newImage))
                            {
                                graphics.Clear(Color.White); // Set the background to white
                                graphics.DrawImage(originalImage, Point.Empty); // Draw the original image on the new image
                                graphics.Dispose();
                            }

                            //Console.WriteLine("Image converted and saved as JPG with white background.");
                            int width = 200; // Replace with the width expected by the model
                            int height = 50;
                            int numChannels = 3;
                            float[] inputBuffer = new float[width * height * numChannels];
                            // Step 3: Decode the base64 image into a byte array
                            //string imagePath = outputPath;
                            //byte[] imageBytes = File.ReadAllBytes(imagePath);
                            var inputName = session.InputMetadata.Keys.First();
                            var inputShape = session.InputMetadata[inputName].Dimensions.ToArray();
                            //var inputBuffer = new List<float>();
                            Bitmap inputImage = newImage;
                            // Assuming the model expects a 3-channel image (RGB) and each channel is 0-255 (unsigned byte)
                            //foreach (var pixel in imageBytes)
                            //{
                            //    inputBuffer.Add(pixel / 255.0f);
                            //}
                            for (int y = 0; y < height; y++)
                            {
                                for (int x = 0; x < width; x++)
                                {
                                    Color pixelColor = inputImage.GetPixel(x, y);
                                    inputBuffer[(y * width + x) * numChannels + 0] = pixelColor.R / 1.0f;
                                    inputBuffer[(y * width + x) * numChannels + 1] = pixelColor.G / 1.0f;
                                    inputBuffer[(y * width + x) * numChannels + 2] = pixelColor.B / 1.0f;
                                    // Add more channels if required by the model
                                }
                            }
                            newImage.Dispose();
                            inputImage.Dispose();

                            // Step 3: Preprocess the image (implement your own preprocessing method)
                            //float[,,] inputMatrix = PreprocessImage(imageBytes);

                            // Step 4: Create the input tensor from the preprocessed image
                            //var inputTensor = new DenseTensor<float>(inputBuffer.ToArray(), inputShape);
                            //var inputTensor = new DenseTensor<float>(inputBuffer, new[] { 1, numChannels, height, width });
                            var inputTensor = new DenseTensor<float>(inputBuffer, new[] { 1, height, width, 3 }); // Assuming NHWC format (batch, height, width, channels)

                            // Step 4: Run inference and get the model's output
                            var inputs = new List<NamedOnnxValue>
                                {
                                    NamedOnnxValue.CreateFromTensor(inputName, inputTensor)
                                };

                            // Run the inference
                            var results = session.Run(inputs);
                            //Console.WriteLine(results);
                            //CtcDecoder.Decode(results);
                            var outputName = session.OutputMetadata.Keys.First();
                            var outputTensor = results.FirstOrDefault(output => output.Name == outputName)?.AsTensor<float>();
                            //float[,,] probabilityMatrix = new float[1, outputTensor.Dimensions[1], outputTensor.Dimensions[2]];
                            string predictedText = CtcDecoder.Decode(outputTensor);

                            // Print the extracted text
                            //Console.WriteLine(predictedText);
                            return predictedText;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"An error occurred: {ex.Message}");
                return ex.Message;
            }
        }
    }
}
